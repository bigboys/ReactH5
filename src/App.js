import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from './Login'
import Order from './Order'

class App extends React.Component {
  render() {

    return (
      <Router>
        <Route exact path="/" component={Login} />
        <Route path="/order" component={Order} />
      </Router>
    );
  }
}

export default App;
