import request from './helper.js'

let ViewUtils = {};
ViewUtils.cookie = 'ARRAffinity1977=c0b5b758297dec82d7f628a0b7baab771f8a3150f92656f415799f66b600ad61; NSC_DUD_hsffnft1_80=ffffffff09090ca145525d5f4f58455e445a4a423660; ASP.NET_SessionId=tzqvgcveikeyj0eiejzf3byj; ARRAffinity176_Gree2008Web_GreeMesextension=37b3ad3b1e81e90b4f61116ee6bd01784f81e42867075e3ff7b9db055eae0ed6; ARRAffinity176_Gree2008Web_Gree=f81e03c46c93bb690f302b6c509437f501b5ce117899701a64fa75086933c9';
ViewUtils.testorderRightNow = (item) => {
    item.checkNum = 1;//
    item.packageNum = 1;
    // console.log(`查询${item.pit == '1' ? 'A' : 'B'}核后返回的值：` + JSON.stringify(item));
    if(isNaN(item.checkNum) || item.checkNum <= 0 || isNaN(item.packageNum) || item.packageNum <=0) {
                // console.log('货号：' + item.id  + `${item.pit == '1' ? 'A' : 'B'}` + '核心查询包装数或可送货数为0，终止下单      ' + ViewUtils.itemToString(item));
    } else {
          console.log('货号：' + item.id + `${item.pit == '1' ? 'A' : 'B'}` +'核有需求，开始查询订单情况      ' + ViewUtils.itemToString(item));
          console.log('计算出来的实际AB核需求为：' + Math.ceil(item.checkNum/item.packageNum) * item.packageNum);
          item.abCoreCheckNum =  Math.ceil(item.checkNum/item.packageNum) * item.packageNum;
          ViewUtils.searchUseableOrder(item);
    }
}
ViewUtils.testSearchOrderDispathMessage = () => {
    let item = {};
    item.id = '400205402';
    item.pit = '1';
    item.checkNum = 18;
    item.packageNum = 18;
    item.orderId = 'P12201724'
    item.lineId = '10';
    item.abCoreCheckNum = '18';
    ViewUtils.searchOrderDispathMessage(item);
}
ViewUtils.initSearchCore = (item) => {
    ViewUtils.searchABCore(item,
        'U86krqP1RvDP7Zm8I8salsac9EbCPX9988LZe6Wd5b6JN8UrzgNcQNAMfzaSp+7VtGpWUXtriyGR9lB8is3owUZOOmFvkpJ8d/4Z32iYeRcSleU7GtxmYXu9Iss/rWO0pQ6sVhNskKzjSoy6yb1zCYNWfFqBOrZAQ73jarqVo5RCTmg8rYy6MaQvjlgF7fXIc5iUE89uC+upbcRWGiR1Opx5g0einYdvcKWGxsZd0H07+fbCrYVl2dMnC9YYbFJwMA3wCR5Ows4H72dgMs0VphRy5IyflF+K+MAT2Tj71eiB+L+V89/UyLzob5A0oStutCAteTyQT0FiR0Vte1YgDzUQYFG3kVdNRxudcL2MxxRCon6+NUdyA9+XyjSBELjPQ7l9nxkLaacNU7WIbn+RAM65mDAY0bnB2d3BWSZ3GcAvv6eezvI/chryINXOQKMs+pE0Ou93cI06CBlzTI0XQHRb4v6W55c/ZNhN6G4JX6pSDG8ZLxrCbcqYIplo71X8bHTayg2JmQjv4t7d6BEbqB7xwbh8ZW1bAiCJNFmvGvwcUIhUOqDNARvuY0DvJPoOintO8GHw/cMmbysMiHS7CSHAw8rhTbGDnj1pHr7iy3fc8aeB+8UiaasIwjlo7F7AA9gB5Sn0o2jXfIQFGH9lAlyrFTjV4d3JECPnU/+XGkS5anmyiTcIxaF7sItDgjE0t4lNMQ==',
        'qcvIVZ6kZNacpp6CRiVDBE7P7foOk1kvRTx8zIgsNNfk2AccfxPK0HWjHMFlFI1eHj3KLDITJmYfMVSpu+3nXPMPStPklbGcvVmZIKXr6jeG8fphU5sD052OiDCQtkzrHEp458lmjZ4cJUQVlgOzvJwG8EA/vVo1ZRB52/CI5kVRYWooKc3WQAX/9YLsklrZAbbHAA==',
        '1F5F44C0')
}

ViewUtils.searchABCore = (item, __VIEWSTATE, __EVENTVALIDATION,__VIEWSTATEGENERATOR) => {
    // console.log("查询可供货数量,id=" + item.id);
        const url = "/api/GreeMesExtension/Report/ENAllowDeliveryQuantitySearch.aspx?CurrentUserID=204270&MINFO=238s241s20s36s239s51s218s233s21s168s36s205s48s113s121s60s";
        let viewtor = "1F5F44C0";        
        let searchParams = new URLSearchParams()
        const mthis = this;
        searchParams.set('__EVENTTARGET', "");
        searchParams.set('__EVENTARGUMENT', "");
        searchParams.set('__VIEWSTATE', __VIEWSTATE);
        searchParams.set('__VIEWSTATEGENERATOR', __VIEWSTATEGENERATOR);
        searchParams.set('__EVENTVALIDATION', __EVENTVALIDATION);
        searchParams.set('TopBar_hidden', "");
        searchParams.set('PARTCODE', item.id);
        searchParams.set('PIT', item.pit);
        searchParams.set('txtCategoryName', '685');
        searchParams.set('btnSearch', '查询');

        let _this = this;
        fetch(url, {
            method: 'POST',
            mode: "cors",
            credentials: 'include',
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                'Cache-Control': 'max-age=0',
                'Connection': 'keep-alive',
                'Cookie': 'ARRAffinity1977=c0b5b758297dec82d7f628a0b7baab771f8a3150f92656f415799f66b600ad61; NSC_DUD_hsffnft1_80=ffffffff09090ca145525d5f4f58455e445a4a423660; ASP.NET_SessionId=tzqvgcveikeyj0eiejzf3byj; ARRAffinity176_Gree2008Web_GreeMesextension=37b3ad3b1e81e90b4f61116ee6bd01784f81e42867075e3ff7b9db055eae0ed6; ARRAffinity176_Gree2008Web_Gree=f81e03c46c93bb690f302b6c509437f501b5ce117899701a64fa75086933c97e',
                'Host': '125.88.19.77',
                'Origin':' http://125.88.19.77',
                'Referer': 'http://125.88.19.77/GreeMesExtension/Report/ENAllowDeliveryQuantitySearch.aspx?CurrentUserID=204270&MINFO=238s241s20s36s239s51s218s233s21s168s36s205s48s113s121s60s',
                'Upgrade-Insecure-Requests': '1'
            },
            body: searchParams
        }).then(function (response) {
            if (response.status === 200) {
                let responsetText = response.text();
               return responsetText;
            }
            const error = new Error(response.statusText)
            error.response = response
            throw error
        })
            .then(function (data) {
                // console.log(data);
                // let div = document.createElement('div');
                // div.setAttribute('dangerouslySetInnerHTML', { __html: '<div id="mydiv">123</div>' });
                // let getValue = div.getElementsByClassName('mydiv');
                let tabRow = /<tr id='UltraWebGrid1_r_0'.+<\/tr>/.exec(data);
                let rowStr = tabRow[0];
                var pattern = /<nobr>(.*?)<\/nobr>/g;
                let result;
                let recordItem = [];
	            while ((result = pattern.exec(rowStr)) != null)  {
                    recordItem.push(result[1]);
                }
                // 物料编码	存储核	物料组	采购员	可送货数量	包装数量
                item.checkNum = parseInt(recordItem[4]);//
                item.packageNum = parseInt(recordItem[11]);
                // console.log(`查询${item.pit == '1' ? 'A' : 'B'}核后返回的值：` + JSON.stringify(item));
                if(isNaN(item.checkNum) || item.checkNum <= 0 || isNaN(item.packageNum) || item.packageNum <=0) {
                    // console.log('货号：' + item.id  + `${item.pit == '1' ? 'A' : 'B'}` + '核心查询包装数或可送货数为0，终止下单      ' + ViewUtils.itemToString(item));
                } else {
                    console.log('货号：' + item.id + `${item.pit == '1' ? 'A' : 'B'}` +'核有需求，开始查询订单情况      ' + ViewUtils.itemToString(item));
                    console.log('计算出来的实际AB核需求为：' + Math.ceil(item.checkNum/item.packageNum) * item.packageNum);
                    item.abCoreCheckNum =  Math.ceil(item.checkNum/item.packageNum) * item.packageNum;
                    ViewUtils.searchUseableOrder(item);
                }
    
            })
            .catch(error => console.log('error is', error));
}

/**
     * 查订单信息
     * @param {*} item 
     * @param {*} pit 
     */
ViewUtils.searchUseableOrder = (item) => {
        let orders = ViewUtils.orderInputValues.get(item.id);
        if(!Array.isArray(orders)){
            console.log(`${item.id}未查找到订单信息` + ViewUtils.itemToString(item));
            return;
        }
        console.log('该物料编码对应的订单信息为：' + JSON.stringify(orders) + '   ' + ViewUtils.itemToString(item));
        for(let i = 0;i<orders.length;i++) {
            let newItem = ViewUtils.copyItem(item);
            let order = orders[i];
            newItem.orderId =order.orderNumber;
            newItem.lineId = order.lineNumber;
            newItem.orderUseable = order.orderUseable;
            ViewUtils.searchOrderDispathMessage(newItem)
        }
    }
/**
 * 根据订单Id搜索出可配送列表项
 */
ViewUtils.searchOrderDispathMessage = (item) => {
    const url = "/api/Gree/Distribution/ControlDeliveryOrderEdit1NOJIT.aspx?EnterpriseID=226066&Mode=New";
    var nowDate=new Date().Format("yyyy-MM-dd HH:mm:ss");
    var nowDateP=new Date().Format("yyyy-MM-dd-HH-mm-ss-256");
    let selectValue = item.pit == '1' ? '231397' : '';
        let searchParams = new URLSearchParams();
        searchParams.set('ctl00$ContentPlaceHolder1$ScriptManager1', 'ctl00$ContentPlaceHolder1$UpdatePanel1|ctl00$ContentPlaceHolder1$AddButton');//固定值
        searchParams.set('ctl00$ContentPlaceHolder1$DeliveryNameTextBox', 'CZJ');//司机名
        searchParams.set('ctl00$ContentPlaceHolder1$PlanArrivalTimeEdit', nowDateP);//计划送达时间
        searchParams.set('ctl00_ContentPlaceHolder1_PlanArrivalTimeEdit_p', nowDateP);//计划送达时间
        searchParams.set('ctl00$ContentPlaceHolder1$CustomerDropDownList', '221372');//固定值
        searchParams.set('ctl00$ContentPlaceHolder1$TruckNoText', 'YS123456');//车号
        searchParams.set('ctl00$ContentPlaceHolder1$RemarkTextBox', '');//
        searchParams.set('ctl00$ContentPlaceHolder1$PurchaseOrderNoText', item.orderId);//订单号
        searchParams.set('ctl00$ContentPlaceHolder1$PurchaseOrderDetailText', item.lineId);//行号
        searchParams.set('ctl00_ContentPlaceHolder1_PurchaseOrderDetailText_p', item.lineId);//行号
        // searchParams.set('ctl00xContentPlaceHolder1xIgOrderGrid', `%3CDisplayLayout%3E%3CStateChanges%3E%3CStateChange%20Type%3D%22ActiveRow%22%20Level%3D%220%22%3E%3C/StateChange%3E%3CStateChange%20Type%3D%22SelectedRows%22%20Level%3D%220%22%20Value%3D%22true%22%3E%3C/StateChange%3E%3CStateChange%20Type%3D%22ChangedCells%22%20Level%3D%220_2%22%20Value%3D%22true%22%3E%3C/StateChange%3E%3CStateChange%20Type%3D%22ChangedCells%22%20Level%3D%220_5%22%20Value%3D%${item.checkNum}%22%3E%3C/StateChange%3E%3C/StateChanges%3E%3C/DisplayLayout%3E`);
        searchParams.set('ctl00xContentPlaceHolder1xIgOrderGrid', '%3CDisplayLayout%3E%3CStateChanges%3E%3C/StateChanges%3E%3C/DisplayLayout%3E');
        searchParams.set('ctl00_ContentPlaceHolder1_WebDialogWindow1_clientState', '[[[[null,0,"0px","15px","400px","200px",1,null,null,null,null,3]],[[[[[null,"提示",null,null]],[[[[[]],[],[]],[{},[]],null],[[[[]],[],[]],[{},[]],null],[[[[]],[],[]],[{},[]],null]],[]],[{},[]],null],[[[[null,null,null,null,null]],[],[]],[{},[]],null]],[]],[{"2":[2,"295px"],"3":[3,"186px"],"4":[1,3],"5":[11,0]},[]],null]');
        searchParams.set('__VIEWSTATE', 'CS4wHH8XyX9GUE5H6zmdvcs1DUmYK9Ha5kXac03Y28g/vjamMcCRc3RcfLF1bUdfje//yN7KoAalkVkGa6EGiWk2EVypPkXq3dQ9KDcL3d5VJnY6jReRDpka5w5MTPRlLdXfEgcrq/5rzMzEyEeUUvCST3TbPy5B4U3G7/PJ1tVHR0+x3SxQrMe+VG0ZI58HddI3RMfKVruWB6G6VQ437ks1PgL44JLwryjXcVrc+q2ul4xV6grePiilit/Exz3edSNDAeK48ClpMAhFOCCV9ic1zfPhTxxvp6IlwFsgwNAyr7metGwxemNy0iSdbIt/azylQRDk1LzSdK1gHzoWGLz8UUXaEgVK8IhNxL7WUdWubVB7vZKt6H4xEaLu9F+cjytfqLIdvMVeW+XFN5eRAEUSsFiS4z18UVqtteq0J4FYLUGrGyWJIMjd65Br1uKfDzDjkXnzz3F5mktksOqmXLcHJtT71QuveYEAxjUMtyDgUzLTa+2swZnUT+esS1swMfRd8bC6SN9Cxs+U43Yu+w0RmWyGVQVmUQ//KWnpsilITteGU+DWN4KlBU/IAtQxj1OeFgrrwCG5wM3jjXjGFtky0Q6tI+7kHUswYYy+GcCBKFmAwV9ZubPNg/cq8P+mcDUMVyaChKVWLVaG0vuT0pnBssjcaRibg0G3QBvy0vGTyIzcoxJIaGGlsonymd9mmhOr7TmzM9HxaEAoHs1JFSw23PO6PCJ3QjhDhllLoKpyo5yEoT3F7vRRx2bxBZpVrjVOCjt+EJJtVY3iMS/AWIvlsTiXJqUH7jplxJV6ro3V2GmdLrbI9PAjjG6VsqN53xjvOuJ6hbfaMF/TKKC+8TF//w8qhDDDRyQrFTOkoXtZm+V9z4NGbPSZIbcm+MJlsNJ3P2RYQh7JAn1gfk7AWeIFmiY7uQww162+sXose5Bb3ISPbgtNPA3CB8nrCJx1W+v2+TjrisUSH07PeoC0N6W+nYOb/KHPP3l7Bzd5M5KbwcxDbAxaz+RzBkSpw0Mb5Xl679SHdTZlNofNuNpQ+5GLDsH5iT7fTcQdAK6r0Cx6eaKi8VpEp+8lw5+5qTNuD35iPkokaScvvBlImqvqnqUHXW4Rx11cUAqx0PdYt0pMc+BRjhwQ46hnDed7mUc8CWaCgzxhbGncg524MT/S25wvsFNbgEkKTMbcmRKcFpXFscD9lJe+1+JosQS4A5JZufx1QZYiA+KEf91Hv9wG704e9cgQ3XA5+M40y9CwdQMMqhyOI2+k06s0yf8qkjRq6gzbHTZe56RGb+csQYbfxunsoTg7Lgx57a9cK4joVj3u68ZFZLR8zo8itAdCV7eO9hIXkueeoBu1RTULmceswGTovo18Vt/MmqofuSewrBAgvxvCsMN8rTnk3Y1Pw46MRn7SRBC3iMBWwRtERUEp4dOOXOrnRksBJIh+fVe81gbZoN8xyAxMV6/YEG9SkDwqWe0YZvnoSgkCuiiTAmij2S5V7U5rS+4TwbIKGsULvSXeq4Tgsrh9mQlZO2VqyO457SyR2x2QyZKd9fZ3v30zxbXYKnbyNuBFfcvM0GDZ/g2sCecGWy5TArKWFYHFst/60+Dzgg63vrT2FrBpZQ/QajlYHC7YYDRZhoj5+gFTHf4cFXB0xIoY4tVHFduDeCjbxpESaw==');
        searchParams.set('__VIEWSTATEGENERATOR', 'FC95245C');
        searchParams.set('__EVENTVALIDATION', 'H+eEt8oc6nBc0BmVcPZDuGfaaLp7Rtb4HGpGSLyl5q5i+n7njuYR9e2jROc5XTnl2QVmNsgV+1pYi1L+9oFTcdwUY3owlexKYYeKik67yC4FU4IU8wIiL2LXClq8J8gNxUSO+ay641JSH3Pszws6aUIbH1q6ZpbrVZGnjLTnVQ+uh8PieyJHNzRBy6+38dc4atI2xnbgOTIf+AGttCd/AMJylKo=');
        searchParams.set('__ASYNCPOST', 'true');
        searchParams.set('ctl00$ContentPlaceHolder1$AddButton', '添加');

        let _this = this;
        fetch(url, {
            method: 'POST',
            mode: "cors",
            credentials: 'include',
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                'Cache-Control': 'no-cache',
            },
            body: searchParams
        }).then(function (response) {
            if (response.status === 200) {
                let responsetText = response.text();
               return responsetText;
            }
            const error = new Error(response.statusText)
            error.response = response
            throw error
        })
            .then(function (data) {
                let __VIEWSTATE = /\|__VIEWSTATE\|(.*?)\|/.exec(data)[1];
                let __EVENTVALIDATION = /\|__EVENTVALIDATION\|(.*?)\|/.exec(data)[1]
                // console.log(data);
                let tabRow = /<tr id='ctl00xContentPlaceHolder1xIgOrderGrid_r_0'.+<\/tr>/.exec(data);
                let rowStr = tabRow[0];
                var pattern = />(.*?)<\/td>/g;
                let result;
                let recordItem = [];
	            while ((result = pattern.exec(rowStr)) != null)  {
                    recordItem.push(result[1]);
                }
                let maxUse = parseInt(recordItem[10]);
                let abCoreCheckNum = parseInt(item.abCoreCheckNum);
                maxUse = Math.min(maxUse,abCoreCheckNum);
                item.maxUse = maxUse;
                console.log(`${item.pit == '1' ? ' A' : ' B'}` +'  订单可用数填写值为' + maxUse +  ViewUtils.itemToString(item));
                ViewUtils.keepOrder(item,maxUse,__VIEWSTATE,__EVENTVALIDATION);
            })
            .catch(error => console.log('error is', error));
}
/**
 * 保存订单
 */
ViewUtils.keepOrder = (item,orderRealNeedCount,__VIEWSTATE,__EVENTVALIDATION)=> {
    const url = "/api/Gree/Distribution/ControlDeliveryOrderEdit1NOJIT.aspx?EnterpriseID=226066&Mode=New";
    var nowDate=new Date().Format("yyyy-MM-dd HH:mm:ss");
    var nowDateP=new Date().Format("yyyy-MM-dd-HH-mm-ss-0");
    // nowDateP = '2019-12-23-14-50-20-0';
    let selectValue = item.pit == '1' ? '231397' : '231406';
    let orderGrid = `<DisplayLayout><StateChanges><StateChange Type="SelectedRows" Level="0" Value="false"></StateChange><StateChange Type="ActiveRow" Level="0"></StateChange><StateChange Type="ChangedCells" Level="0_5" Value="${orderRealNeedCount}"></StateChange></StateChanges></DisplayLayout>`
        let searchParams = new URLSearchParams();
        searchParams.set('ctl00$ContentPlaceHolder1$ScriptManager1', 'ctl00$ContentPlaceHolder1$UpdatePanel2|ctl00$ContentPlaceHolder1$OKButton');//固定值
        searchParams.set('ctl00$ContentPlaceHolder1$DeliveryNameTextBox', 'CZJ');//司机名
        searchParams.set('ctl00$ContentPlaceHolder1$PlanArrivalTimeEdit', nowDate);//计划送达时间
        searchParams.set('ctl00_ContentPlaceHolder1_PlanArrivalTimeEdit_p', nowDateP);//计划送达时间
        searchParams.set('ctl00$ContentPlaceHolder1$CustomerDropDownList', '221372');//固定值
        searchParams.set('ctl00$ContentPlaceHolder1$TruckNoText', 'YUES123456');//车号
        searchParams.set('ctl00$ContentPlaceHolder1$RemarkTextBox', '');//
        searchParams.set('ctl00$ContentPlaceHolder1$PurchaseOrderNoText', item.orderId);//订单号
        searchParams.set('ctl00$ContentPlaceHolder1$PurchaseOrderDetailText', item.lineId);//行号
        searchParams.set('ctl00_ContentPlaceHolder1_PurchaseOrderDetailText_p', item.lineId);//行号
        searchParams.set('ctl00xContentPlaceHolder1xIgOrderGrid', encodeURIComponent(orderGrid));
        searchParams.set('ctl00$ContentPlaceHolder1$TargetEquipmentDDList', selectValue);
        //下拉框的值,231397 1251010物资中心5楼东莞怡盛、685124、杨建和
        //231406 3131018双子座A座3东莞怡盛、685124、常柱金
        //312945 3131292双子座A座3楼储备物资库位、常柱金
        searchParams.set('ctl00_ContentPlaceHolder1_WebDialogWindow1_clientState', '[[[[null,3,"0px","15px","400px","200px",1,null,null,null,null,0]],[[[[[null,"提示",null,null]],[[[[[]],[],[]],[{},[]],null],[[[[]],[],[]],[{},[]],null],[[[[]],[],[]],[{},[]],null]],[]],[{},[]],null],[[[[null,null,null,null,null]],[],[]],[{},[]],null]],[]],[{},[]],null]');
        searchParams.set('__VIEWSTATE', __VIEWSTATE);
        searchParams.set('__VIEWSTATEGENERATOR', 'FC95245C');
        searchParams.set('__EVENTVALIDATION', __EVENTVALIDATION);
        searchParams.set('__ASYNCPOST', 'true');
        searchParams.set('ctl00$ContentPlaceHolder1$AddButton', '保存并关闭');

        let _this = this;
        fetch(url, {
            method: 'POST',
            mode: "cors",
            credentials: 'include',
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                'Cache-Control': 'no-cache',
                'pragma': 'no-cache',
                'Cookie': 'ASP.NET_SessionId=xuladmff1yhswtmr3dw4io55; ARRAffinity176_Gree2008Web_Gree=37b3ad3b1e81e90b4f61116ee6bd01784f81e42867075e3ff7b9db055eae0ed6; NSC_DUD_hsffnft1_80=ffffffff09090ca145525d5f4f58455e445a4a423660; ARRAffinity1977=249c24e5ca3fc0d86001a1da8adab9ab07544fc12dbd05a51d6ab23a68422b91',
                'x-microsoftajax': 'Delta=true',
                'Content-Type':'application/x-www-form-urlencoded; charset=utf-8'
            },
            body: searchParams
        }).then(function (response) {
            if (response.status === 200) {
                let responsetText = response.text();
               return responsetText;
            }
            const error = new Error(response.statusText)
            error.response = response
            throw error
        })
            .then(function (data) {
                if(data.indexOf("当前量不能大于可用量或者为") != -1) {
                    console.log(ViewUtils.itemToString(item) + ' 核心：' + item.pit + '当前量不能大于可用量或为0');
                } else if(data.indexOf("物料最多可送货")!= -1) {
                    console.log(ViewUtils.itemToString(item) + ' 核心：' + item.pit + '物料最多可送货');
                } else {
                    console.log('好像成功了' + ViewUtils.itemToString(item))
                }
            })
            .catch(error => console.log('error is', error));
}

ViewUtils.itemToString = (item) => {
    let {pit, id, checkNum, packageNum, orderId, orderRealNeedCount , lineId, abCoreCheckNum} = item;
    return `(${pit == '1' ? 'A核 ' : 'B核心 '} 物料编码：${id} ${checkNum != undefined ? '可送货数量：' + checkNum : ''} ${packageNum!= undefined ? '包装数' + packageNum : ''} ${abCoreCheckNum!= undefined ? '计算出来的可打单数' + abCoreCheckNum : ''} ${orderId!= undefined ? '关联订单号:' + orderId : ''} ${lineId!= undefined ? '行号:' + lineId : ''}  ${orderRealNeedCount!= undefined ? '订单可用数' + orderRealNeedCount: ''}})`
}
ViewUtils.copyItem = (item) => {
    let {pit, id, checkNum, packageNum, orderId, orderRealNeedCount , lineId, abCoreCheckNum} = item;
    let newItem = {};
    newItem.pit = pit;
    newItem.id = id;
    newItem.checkNum = checkNum;
    newItem.packageNum = packageNum;
    newItem.orderId = orderId;
    newItem.orderRealNeedCount = orderRealNeedCount;
    newItem.lineId = lineId;
    newItem.abCoreCheckNum = abCoreCheckNum;
    return newItem;
}
export default ViewUtils;