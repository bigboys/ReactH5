import React, { Component } from 'react';
import request from './helper.js'
import './style.css';
import { Redirect } from "react-router-dom";
import { Button, Radio, Dialog } from '@alifd/next';
import ViewUtils from './GetSearchViewValue.js';


// var viewstate = 'vlcSk4S4f8r13QhnRCY3F CTdCPb hYTVyWAWReB2MiujXASRx7S8y xl8xMN/bq0Vzjq2ZQYeegwIXHIod4X5Pb2Nk6HWR OSZv8Dd2LVhNUd1DuxZDyUoGfjfbox46hFxytUVejTZV4sCdxojs5/kan VOpyyansK1jnJuKl6i3fYnAh8QOaXZnxrZsi42l1lgYlybO653vAk5Gg QmGp1b17cxfA89D4FROEAlkH6eDWVk2dfHu2e YhlxaRHGS/GAcIv7iCC5iO/Meb9D4XWmsgLLAlVi/B6lYUuJsmI3JqMQacDmPXJtNx7AlSYOgHYzpyjY30M0skaTvsGEEP7/tUEKyUOpgW7BAc05ogpRjXbiAQqC/eqdi4vivoI 5RwhiAK8RNynXvFshhRk4eeD MWG4T6UhDeNVK98zP5LXl04lt1ZJnFBc/28T2zDfLLUp1kW6Ckqlg7XSOAibJD1cuxomcfhRjfHxpYGKQmfJeIHI8o3eyBRKmshoWxl0wHreP4/YVWncqg7jGq X8s gg3fDRsLIeChfthBISnybyIY0tuKue6CrYxQTxu6Yw8/LD Uq8zKAz9aRecNgg1KruDsTXIkeb rEeXnua3r6b2KoGLwL9 Hjv1nxj8ET2 V PV9QxQNIG2ESfiHKrW 8pTSVcek7UUaaLpWlsEAJAop F1chY8FKolHZLkAU1HnPbo llwX3MdPa5u8ikwhxnn5W6e0wsd I08Un/FQgGVsYDVZgIyNkCDwWWzvNQu9Zy41iFrU2YyunlV7oy6MrUm9ZZIH70MKnDPsX8t3bNAeNzVMcJf4xvyOoiE2DY5 hkg8eepZwdjYoDAZnIYi7cWggg2dTF37hvXvzBg4ykECfWwKzcouPFBWWWTZwtob843jIQ0a0oW ipXL1baaF8YFpt6hbS/JW0CB8qfYIJAmBhNgQ5mJeBVcy3d8T2mChwkVdOCtYzd0pH5BKHHKbcmSi7Z857 9rsRrI5iXof1fLepISOJI4R 1b5hoIlhQssgLlihWjFeFqbqsIhLzZ38bhS60l5L/WFQKwQ2PyZZF7iD2c5wg5GWnrzHhs9WMg8HlPqZgP73DXH hhlkd4kv55KEXesok9uwjVCN/GXMt5ZZNWouI7qXftIPRPmm57B0bxfV8b2PiqyWUkRun0cmsFGBezscqESthwJXsmH/0RrsW0H63bCcvPKKmMcm9KDKYqY 2xTIMXHXJy8UNC7V4yoOEPfNYUTIx5NIqgSQLWKt5oNe0j4yN1hyhhwaAqGksAWfmPHgydsP5ppF 5kZKngih q6cULCur8MX/2wPTyWSgj OO52eMJzzjO xbpm6K9AZTlEQxAsXDgAVHKOUSz0F/vJ8uyo0iB0zoR4G5l4yFNaamnqq3JjQ8ITvdSS/8pjHSKSWOuTK5GnLn8=';
// var viewtor = '1F5F44C0';
// var viewtion = 'CFS4fQAkMUngH89wiU1wuOsOfZ08q1to/qOOAM9C2pwLt0t6ciTAilpW7/Mh9MwB1a2shpNGY3h TE9oVxoQ/JIGmPZEqtTXa9cVZBGd/qasYYQlGpc056V5yIOeJAcn Hmb4ANSyMBg9 zwwxY82iUhozc27b3rXBlTDipKHrj4GP14jjiAr/FT3U zr8rpcAuJNA==';
Date.prototype.Format = function(fmt){
    var o = {
        "M+": this.getMonth()+1,
        "d+": this.getDate(),
        "H+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "S+": this.getMilliseconds()
    };
    //因为date.getFullYear()出来的结果是number类型的,所以为了让结果变成字符串型，下面有两种方法：
    if(/(y+)/.test(fmt)){
        //第一种：利用字符串连接符“+”给date.getFullYear()+""，加一个空字符串便可以将number类型转换成字符串。
        fmt=fmt.replace(RegExp.$1,(this.getFullYear()+"").substr(4-RegExp.$1.length));
    }
    for(var k in o){
        if (new RegExp("(" + k +")").test(fmt)){
            //第二种：使用String()类型进行强制数据类型转换String(date.getFullYear())，这种更容易理解。
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(String(o[k]).length)));
        }
    }
    return fmt;
};
let orderInterval;
let orderStatus = false;
export default class Order extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            metrailValues: [],
            orderValue: [],
            searchHtml: '',
        }
    }


    render() {
        return (
            <div className="container">
                <div className="div_left">
                    <h1>订单界面</h1>
                    <textarea className="inputMertailInput" id="inputMertailInput" value="G1-40020243	0	1500
G1-400202456	0	273
G1-40020317	0	2275
G1-400203175	0	1560
G1-40020318	0	325
G1-40020403	0	350
G1-400204052	0	50
G1-400204054	0	3343
G1-400204055	0	165
G1-400204055	胶筐	825
G1-400204058	0	89
G1-4002041709	0	479
G1-40020419	0	554
G1-400205223	0	462
G1-4002052333	胶筐	840
G1-4002052333	0	869
G1-400205235	0	588
G1-400205322	0	1098
G1-400205322	胶筐	1190
G1-400205352	0	5730
G1-400205358	0	726
G1-40020536	胶筐	996
G1-40020536	0	7857
G1-4002053602	0	564
G1-4002053603	0	3777
G1-4002053604	胶筐	530
G1-4002053604	0	2157
G1-40020538	胶筐	317
G1-40020538	0	843
G1-4002053813	0	428
G1-400205382	胶筐	1089
G1-400205382	0	5428
G1-400205393	0	144
G1-400205397	0	462
G1-400205399	0	154
G1-40020540	胶筐	6891
G1-40020540	0	7300
G1-400205401	胶筐	593
G1-400205401	0	2958
G1-4002054010	0	1907
G1-4002054014	0	1440
G1-400205402	0	1210
G1-4002054026	胶筐	1170
G1-4002054026	0	1689
G1-400205403	0	469
G1-400205403	胶筐	1068
G1-400205407	0	8404
G1-400205472	0	50
G1-400205472	胶筐	128
G1-4002078011	0	99
G1-4002078018	0	1290
G1-4002078019	0	138
G1-4002078028	胶筐	5240
G1-4002078028	0	8600
G1-4002078050	0	975
G1-40020789	0	2140
G1-40030184	0	822
G1-40030209	胶筐	1120
G1-40030209	0	1890
G1-40030311	0	1950
G1-40030311	胶筐	2410
G1-40030313	0	791
G1-40030318	0	1401
G1-4003032913	0	4
G1-4003032914	0	48
G1-4003032921	胶筐	4410
G1-4003032922	0	2835
G1-400303299	0	2242
G1-400303299	胶筐	2350
G1-40032134	0	4030
G1-4003213450	0	169
G1-4003213451	0	2518
G1-4003213484	0	60
G1-410103060004	0	3720
G1-410103060005	0	9895
G1-410103060006	0	3707
G1-410103060023	0	828">
                    </textarea>
                    <textarea className="inputMertailInput" id="inputOrder"  value="P12196733	2	40020243	1500
P12200732	2	40020317	13175
P12196733	4	400203175	500
P12202330	2	400203175	1060
P12209654	2	40020403	80
P12213889	2	40020403	270
P12212739	2	400204052	500
P12214300	2	400204052	500
P12203086	2	400204054	130
P12203649	2	400204054	270
P12207370	2	400204054	650
P12207778	2	400204054	3170
P12209654	6	400204054	1850
P12210827	2	400204054	1850
P12212739	4	400204054	2380
P12213392	2	400204054	120
P12213889	4	400204054	270
P12214300	4	400204054	1060
P12203086	4	400204055	695
P12206409	2	400204055	210
P12207370	4	400204055	1850
P12207897	2	400204055	60
P12214300	6	400204055	1590
P12209351	2	400204058	790
P12202330	6	40020419	448
P12207370	6	40020419	340
P12200732	4	400205233	13200
P12203086	6	4002052333	630
P12209351	4	4002052333	670
P12209654	8	4002052333	340
P12207897	4	400205235	520
P12212603	2	400205235	1500
P12205426	2	400205322	95
P12207370	8	400205322	150
P12207897	6	400205322	270
P12209654	10	400205322	1810
P12211669	2	400205322	1370
P12214300	8	400205322	270
P12196733	6	400205326	2500
P12196733	8	400205352	245
P12202330	12	400205352	3128
P12209351	8	400205352	1320
P12196733	10	40020536	713
P12205937	4	40020536	530
P12206409	4	40020536	270
P12207066	4	40020536	270
P12207778	4	40020536	100
P12207897	8	40020536	5910
P12210366	4	40020536	1060
P12212603	4	40020536	800
P12212739	6	40020536	1060
P12213392	4	40020536	3440
P12214300	10	40020536	3220
P12212603	6	4002053603	80
P12212739	8	4002053603	530
P12214300	12	4002053603	2910
P12203086	10	4002053604	230
P12203649	4	4002053604	57
P12207370	10	4002053604	1380
P12207897	10	4002053604	120
P12209654	12	4002053604	1590
P12210827	4	4002053604	30
P12211261	2	4002053604	270
P12213392	6	4002053604	1060
P12201724	6	40020538	70
P12204798	6	40020538	270
P12204877	2	40020538	200
P12207066	8	40020538	460
P12207370	12	40020538	270
P12207897	12	40020538	270
P12210366	6	40020538	270
P12210827	6	40020538	270
P12211261	4	40020538	460
P12211669	4	40020538	800
P12212603	8	40020538	530
P12212739	10	40020538	1060
P12213392	8	40020538	270
P12213889	6	40020538	530
P12214300	14	4002053813	195
P12196733	12	400205382	1395
P12207778	10	400205382	240
P12207897	14	400205382	530
P12209654	14	400205382	530
P12210366	8	400205382	3250
P12212739	12	400205382	270
P12213392	12	400205382	1320
P12214300	16	400205382	800
P12202330	14	40020539	670
P12200091	8	400205397	12
P12200732	6	400205397	60
P12202330	16	400205397	300
P12200732	8	40020540	308
P12204798	8	40020540	680
P12205937	6	40020540	210
P12207066	12	40020540	882
P12207370	14	40020540	1260
P12207778	12	40020540	420
P12207897	18	40020540	2640
P12209654	16	40020540	1890
P12211669	6	40020540	1680
P12212603	10	40020540	210
P12212739	14	40020540	1260
P12213392	14	40020540	3150
P12214300	18	40020540	2520
P12200091	12	400205401	326
P12201315	2	400205401	120
P12207370	16	400205401	420
P12207778	14	400205401	90
P12209654	18	400205401	630
P12210366	10	400205401	210
P12211261	6	400205401	420
P12211669	8	400205401	420
P12212603	12	400205401	630
P12212739	16	400205401	420
P12213392	16	400205401	180
P12213889	8	400205401	840
P12214300	20	400205401	2100
P12203649	6	4002054010	270
P12204798	10	4002054010	20
P12205426	4	4002054010	50
P12207066	14	4002054010	530
P12207370	18	4002054010	270
P12207778	16	4002054010	50
P12207897	20	4002054010	800
P12210366	12	4002054010	270
P12210827	8	4002054010	1320
P12211669	10	4002054010	1640
P12212603	14	4002054010	3170
P12212739	18	4002054010	530
P12213392	18	4002054010	270
P12213889	10	4002054010	1060
P12214300	22	4002054010	530
P12204798	12	4002054014	80
P12210827	10	4002054014	170
P12213889	12	4002054014	260
P12201724	10	400205402	120
P12203086	16	400205402	270
P12206409	8	400205402	110
P12209654	22	400205402	270
P12210366	14	400205402	270
P12212739	20	400205402	530
P12213889	14	400205402	270
P12203086	18	4002054026	25
P12206409	10	4002054026	130
P12207066	16	4002054026	410
P12207778	18	4002054026	820
P12209654	24	4002054026	820
P12210366	16	4002054026	820
P12210827	12	4002054026	410
P12212739	22	4002054026	3280
P12213889	16	4002054026	410
P12214300	24	4002054026	4100
P12204005	2	400205403	270
P12204798	14	400205403	270
P12205426	6	400205403	270
P12209654	26	400205403	700
P12210366	18	400205403	530
P12211261	8	400205403	270
P12212739	24	400205403	530
P12213392	20	400205403	800
P12213889	18	400205403	1060
P12214300	26	400205403	2120
P12200091	18	400205407	407
P12204005	4	400205407	115
P12204798	16	400205407	170
P12205426	8	400205407	1410
P12205937	10	400205407	1590
P12207066	18	400205407	270
P12207370	20	400205407	800
P12207778	20	400205407	270
P12211669	16	400205407	620
P12212739	26	400205407	3960
P12213889	20	400205407	800
P12214300	28	400205407	270
P12200091	20	4002078018	400
P12200732	10	4002078018	120
P12200091	22	4002078028	4400
P12203086	20	4002078028	3360
P12203649	8	4002078028	420
P12205426	10	4002078028	5250
P12206409	14	4002078028	210
P12207778	22	4002078028	210
P12202330	22	4002078050	920
SYP034719	2	4002078056	2
P12196733	16	40020789	800
P12205426	12	40030184	65
P12207370	22	40030184	20
P12202330	26	40030209	270
P12205426	14	40030209	530
P12205937	12	40030209	1060
P12206409	16	40030209	270
P12207778	24	40030209	530
P12209351	10	40030209	350
P12202330	28	40030311	4130
P12209351	12	40030311	530
P12196733	20	40030318	100
P12202330	30	40030318	1270
P12209351	14	40030318	5020
P12202330	32	4003032921	3360
P12202330	34	4003032922	7140
P12196733	22	400303299	424
P12200919	2	400303299	420
P12202330	36	400303299	5220
P12209351	18	400303299	7130
P12202330	38	40032117	70
P12202330	40	40032134	800
P12203649	10	40032134	800
P12204798	18	40032134	50
P12209351	20	40032134	2380
P12209654	28	40032134	530
P12210366	20	40032134	530
P12210366	22	4003213450	270
P12211261	10	4003213450	230
P12211669	18	4003213450	270
P12212739	28	4003213450	270
P12207897	26	40032142	10
P12202330	42	410103060005	11070
SYP039271	2	410103060032	5
">

                    </textarea>
                    <button onClick={this.parseMaterialValue}>解析物料编码</button>
                    <h4>库存列表</h4>

                    <div className="meterial_item_container">
                        <div className="meterial_col">货号</div>
                        <div className="meterial_col">批号</div>
                        <div className="meterial_col"> 数量</div>
                        <div className="meterial_col"> 缺货数</div>
                    </div>
                    <div className="meterial_item">
                        {this.state.metrailValues.map((item, i) => <MtterialItem key={i} item={item} />)}
                    </div>
                </div>

                <div class="div_right">
                    <div>
                        <button onClick={this.startGetApi} >开始抢单</button>
                        <button onClick={() => {
                            orderStatus = false;
                            clearInterval(orderInterval);
                            alert('抢单已经暂停');
                        }} >暂停抢单</button>
                    </div>
                    <h4>抢单成功列表</h4>
                    <div className="sty1">
                        <div className="sty1">货号</div>
                        <div className="sty1">数量</div>
                        <div className="sty1"> 包装数</div>
                        <div className="sty1"> 仓库数量</div>
                    </div>
                </div>
            </div>

        );
    }

    parseMaterialValue = () => {
        let input = document.getElementById('inputMertailInput');
        let inputValue = input.value;
        if (inputValue === null || inputValue === '') {
            alert('请粘贴物料编码');
            return;
        }
        let rows = inputValue.split('\n');
        console.log(rows);
        let items = [];
        for (let i = 0; i < rows.length; i++) {
            let col = rows[i].split('\t');
            let id = col[0].replace('G1-', '');
            let batchNum = col[1];
            let inventory = parseInt(col[2]);
            if (!id || !batchNum || isNaN(inventory)) {
                continue;
            }
            let item = {};
            item.id = id;
            item.batchNum = batchNum;
            item.inventory = inventory;
            item.pit = '1';


            let itemB = {};
            itemB.id = id;
            itemB.batchNum = batchNum;
            itemB.inventory = inventory;
            itemB.pit = '2';

            items.push(item);
            items.push(itemB);
        }
        let orderInputStr = document.getElementById('inputOrder').value;
        let orderRows = orderInputStr.split('\n');
        let orders = new Map();
        for(let i = 0;i< orderRows.length;i++) {
            let col = orderRows[i].split('\t');
            let item = {};
            let orderNumber = col[0];
            let lineNumber = col[1];
            let orderUseable = col[3];
            let id = col[2];
            if(!id || !orderNumber || !lineNumber || !orderUseable)
                continue;
            item.orderNumber = col[0];
            item.lineNumber = col[1];
            item.id = col[2];
            item.orderUseable = orderUseable;

            let orderWithItems = orders.get(id);
            if(!Array.isArray(orderWithItems)) {
                orderWithItems = [];
                orders.set(id,orderWithItems);
            }     
            orderWithItems.push(item);
        }
        console.log(items)
        this.state.metrailValues = items;
        this.state.orderValue = orders;
        ViewUtils.orderInputValues = orders;
        console.log(this.state.orderValue);
        this.setState({
            metrailValues: items
        })
    }
    componentDidUpdate() {
        
    }
    startGetApi = () => {
        if(orderStatus) {
            alert('已经在抢单了');
            return;
        }
        let items = this.state.metrailValues;
        if (items === null || items === '' || items.length == 0) {
            alert('请粘贴物料编码');
            return;
        }
        orderStatus = true;
        let i = 0;
        orderInterval = setInterval(() => {
            if(i >= items.length)
                i = 0;
            let item = items[i++];
            if(item.inventory == 0) {
                console.log('批号' + item.id + '为空')
            } else {
                // console.log('批号' + item.id + '开始抢单')
                ViewUtils.initSearchCore(item, "1");
                // ViewUtils.initSearchCore(item, "2");
            }
        }, 200);
        // ViewUtils.testSearchOrderDispathMessage();
    }

    /**
     * 查询AB核里是否需要这个货号的货物
     */
    queryNeedCount = (item, pit) => {
        
    }
}



class MtterialItem extends React.Component {
    render() {
        const { item } = this.props
        return (
            <div className="meterial_item_container">
                <div className="meterial_col"> {item.id}</div>
                <div className="meterial_col"> {item.batchNum}</div>
                <div className="meterial_col"> {item.inventory}</div>
                <div className="meterial_col"> 缺少：0</div>
                <hr />
            </div>
        )
    }
}


