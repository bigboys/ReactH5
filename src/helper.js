const commonUrl = 'http://125.88.19.77/Gree'
localStorage.setItem("access_token","test")

function parseJSON(response){
  return response.json()
}

function checkStatus(response){
  if(response.status >= 200 && response.status < 500){
    return response
  }
  const error = new Error(response.statusText)
  error.response = response
  throw error
}

export default  function request(options = {}){
  // const Authorization = localStorage.getItem('access_token')
  const {data,url} = options
  options = {...options}
  options.mode = 'cors'
  delete options.url
  if(data){
    delete options.data
    options.body = JSON.stringify({
      data
    })
  }
  options.headers={
    // 'Authorization':Authorization,
    'Content-Type':'application/json'
  }
  return fetch(commonUrl+url,options,{credentials: 'include'})
    .then(checkStatus)
    .then(parseJSON)
    .catch(err=>({err}))
}

export  function requestForm(options = {}){
  // const Authorization = localStorage.getItem('access_token')
  const {data,url} = options
  options = {...options}
  options.mode = 'cors'
  delete options.url
  if(data){
    delete options.data
    options.body = JSON.stringify({
      data
    })
  }
  options.headers={
    // 'Authorization':Authorization,
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  }
  return fetch(commonUrl+url,options,{credentials: 'include'})
    .then(checkStatus)
    .then(parseJSON)
    .catch(err=>({err}))
}