const proxy = require('http-proxy-middleware');
 
module.exports = function(app) {
  app.use(proxy('/api', { 
    target: 'http://125.88.19.77',
    changeOrigin:true,
    pathRewrite: {
                "^/api": "/"
            }
     }));

};